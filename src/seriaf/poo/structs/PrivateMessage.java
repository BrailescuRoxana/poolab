/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.structs;

/**
 *
 * @author student
 */
public class PrivateMessage extends Message {

    private String reciver;

    public PrivateMessage(String sender1, String content1, String reciver1) {
        super(sender1, content1);
        reciver = reciver1;

    }

    public String toString() {
        String priv = null;
        return "(priv) " + super.toString();
    }

    public String getReciver() {
        return reciver;
    }

}
