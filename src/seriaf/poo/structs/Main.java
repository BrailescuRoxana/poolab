/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.structs;

/**
 *
 * @author professor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PrivateMessage m1 = new PrivateMessage("John", "Hi!", "bye");
        PrivateMessage m2 = new PrivateMessage("Mary", "Hello!", "hi");
        PrivateMessage m3 = new PrivateMessage("John", "Bye!", "hello");

        System.out.printf("%s\n", m1.toString());
        System.out.printf("%s\n", m2.toString());
        System.out.printf("%s\n", m3.toString());

        System.out.printf("%s\n", m1.getReciver());
        System.out.printf("%s\n", m2.getReciver());
        System.out.printf("%s\n", m3.getReciver());
    }

}
