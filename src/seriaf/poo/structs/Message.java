/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.structs;

/**
 *
 * @author professor
 */
public class Message {

    private String Sender;
    private String Content;

    public Message(String sender, String content) {
        Sender = sender;
        Content = content;
    }

    @Override
    public String toString() {
        return Sender + ": " + Content + "\n";
    }
}
